<?php

namespace App\Entity;

use App\Repository\ActualiteRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ActualiteRepository::class)]
class Actualite
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    private $titre_art;

    #[ORM\Column(type: 'date', nullable: true)]
    private $date_art;

    #[ORM\Column(type: 'text', nullable: true)]
    private $description_art;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $visibilite_art;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreArt(): ?string
    {
        return $this->titre_art;
    }

    public function setTitreArt(?string $titre_art): self
    {
        $this->titre_art = $titre_art;

        return $this;
    }

    public function getDateArt(): ?\DateTimeInterface
    {
        return $this->date_art;
    }

    public function setDateArt(?\DateTimeInterface $date_art): self
    {
        $this->date_art = $date_art;

        return $this;
    }

    public function getDescriptionArt(): ?string
    {
        return $this->description_art;
    }

    public function setDescriptionArt(?string $description_art): self
    {
        $this->description_art = $description_art;

        return $this;
    }

    public function getVisibiliteArt(): ?bool
    {
        return $this->visibilite_art;
    }

    public function setVisibiliteArt(?bool $visibilite_art): self
    {
        $this->visibilite_art = $visibilite_art;

        return $this;
    }
}
